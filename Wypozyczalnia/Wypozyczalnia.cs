﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wypozyczalnia
{
    public class Wypozyczalnia
    {
        public List<User> uzytkownicy { get; set; } = new List<User>();

        public void DisplayUsers()
        {
            foreach (User uzytkownik in uzytkownicy)
            {
                Console.WriteLine("Imie: " + uzytkownik.Name + " Nazwisko: " + uzytkownik.Surname + " ID: " + uzytkownik.Id);               
            }
        }
    }
}
