﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wypozyczalnia
{
    public class Init
    {
        Wypozyczalnia spis = new Wypozyczalnia();

        public void MenuInit()
        {
            int wybor;
            do
            {
                Console.WriteLine("WITAMY W WYPOZYCZALNI");
                Console.WriteLine("--------------------------");
                Console.ReadKey();
                Console.Clear();

                Console.WriteLine("MENU");
                Console.WriteLine("--------------------------");
                Console.WriteLine("1. Uzytkownicy");
                Console.WriteLine("2. Filmy");
                Console.WriteLine("3. Wyjscie");
                Console.WriteLine("--------------------------");
                Console.WriteLine("Co chcesz wybrac?");

                wybor = int.Parse(Console.ReadLine());

                switch (wybor)
                {
                    case 1:
                        {
                            User();
                            break;
                        }
                    case 2:
                        {
                            break;
                        }
                    case 3:
                        {
                            return;
                        }
                }
            } while (wybor != 3);
            
        }

        public void User()
        {
            int wybor;

            do
            {
                Console.Clear();
                Console.WriteLine("MENU UZYTKOWNIK");
                Console.WriteLine("--------------------------");
                Console.WriteLine("1. Aktualny uztkownik");
                Console.WriteLine("2. Dodaj uzytkownika");
                Console.WriteLine("3. Usun uzytkownika");
                Console.WriteLine("4: Lista uzytkownikow");
                Console.WriteLine("5. Wyjscie do menu glownego");
                Console.WriteLine("--------------------------");
                Console.WriteLine("Co chcesz wybrac?");
                wybor = int.Parse(Console.ReadLine());

                switch (wybor)
                {
                    case 1:
                        {
                            break;
                        }
                    case 2:
                        {
                            NewUser();
                            break;
                        }
                    case 3:
                        {
                            break;
                        }
                    case 4:
                        {
                            UsersBook();
                            break;
                        }
                    case 5:
                        {
                            break;
                        }
                }


            } while (wybor != 5);
        }
       
            public void NewUser()
        {
            User uzytkownik = new User();
            Console.WriteLine("DODAJ UZYTKOWNIKA:");
            Console.WriteLine("Podaj imie: ");
            uzytkownik.Name = Console.ReadLine();
            Console.WriteLine("Podaj nazwisko");
            uzytkownik.Surname = Console.ReadLine();
            Console.WriteLine("Podaj ID");
            uzytkownik.Id = int.Parse(Console.ReadLine());
            spis.uzytkownicy.Add(uzytkownik);
        }

       /* public void RemoveUser()
        {
            int UserNr;
            Console.WriteLine("Podaj Id uzytkownika ktorego chcesz usunac: ");
            UserNr = int.Parse(Console.ReadLine());
            spis.uzytkownicy.RemoveAt(UserNr - 1);
            Console.ReadKey();
        }*/


        public void UsersBook()
        {
            spis.DisplayUsers();
            Console.ReadKey();
        }
        
    }
}
